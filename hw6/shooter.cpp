#include<iostream>
#include<vector>
#include<string>
#include<msgpack.hpp>

using namespace std;
using namespace msgpack;

void ReadInput(unpacker& unpack,string fname){
    FILE* fin = fopen(fname.c_str(),"rb");
    fseek(fin,0,SEEK_END);
    int fsize=int(ftell(fin));
    rewind(fin);
    unpack.reserve_buffer(fsize);
    fread(unpack.buffer(),fsize,1,fin);
    unpack.buffer_consumed(fsize);
    fclose(fin);
}

void WriteOutput(sbuffer &sbuff,string fname){
    FILE* fout = fopen(fname.c_str(),"wb");
    fwrite(sbuff.data(),sbuff.size(),1,fout);
    fclose(fout);
}

vector<int> targets;
vector<int> dp_route;
vector<int> dp_shoot;
long long int dp_max[4];

void dp_init(int targets_len){
    dp_max[(targets_len-1)&3]=targets[(targets_len-1)];
    dp_shoot[(targets_len-1)]=targets_len;
    if (targets_len>=2){
        dp_max[(targets_len-2)&3]=targets[(targets_len-2)];
        dp_shoot[(targets_len-2)]=targets_len;
    }
    if (targets_len>=3){
        dp_max[(targets_len-3)&3]=targets[(targets_len-1)]+targets[(targets_len-3)];
        dp_shoot[(targets_len-3)]=(targets_len-1);
    }
}
void dp_build(int targets_len){
    for (int i=targets_len-4;i>-1;i--){
        int index2=(i+2)&3;
        int index3=(i+3)&3;
        if (*(dp_max+index2)>*(dp_max+index3)){
            dp_shoot[i]=i+2;
            dp_max[i&3]=targets[i]+dp_max[index2];
        }else{
            dp_shoot[i]=i+3;
            dp_max[i&3]=targets[i]+*(dp_max+index3);
        }
    }
}

void dp_choose(int index){
    int lenth=targets.size();
    for (;index<lenth;index=dp_shoot[index]){
        if (targets[index]){
            dp_route.push_back(index+1);
        }
    }
}

int main(){
    int count;
    unpacker unpack;
    unpacked res;
    sbuffer sbuff;
    ReadInput(unpack,"input.txt");
    unpack.next(&res);
    res.get().convert(&count);
    while(count--){
        long long int max;
        unpack.next(&res);
        res.get().convert(&targets);
        int targets_len=targets.size();
        dp_shoot.clear();
        dp_route.clear();
        dp_shoot.reserve(targets_len);
        dp_init(targets_len);
        dp_build(targets_len);
        if (targets_len>1){
            if (dp_max[1]>dp_max[0]){
                max=dp_max[1];
                dp_choose(1);
            }else{
                max=dp_max[0];
                dp_choose(0);
            }
        }else {
            max=dp_max[0];
            dp_choose(0);
        }
        pack(&sbuff,max);
        pack(&sbuff,dp_route);
    }
    WriteOutput(sbuff,"output.txt");
    return 0;
}
