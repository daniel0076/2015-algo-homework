//546.05M  260ms  465.99MB 80.76K 318.96MB 5/27 23:37:42
//10.88G   2s     82.04MB  2.82K  9.88 MB 5/28 10:13:59
//4.89 G   1s     90.04 MB 2.74K 9.66 MB 5/28 10:45:10
//4.28 G   1s     90.04 MB 2.75K 9.73 MB 5/28 11:08:16
//3.8 G    910ms 84.16 MB 2.75K 9.71 MB 5/28 11:47:16
//2.62G    650ms 79.91 MB 2.72K 7.59 MB 5/29 02:11:43
//以上為top-down dynamic programming
//以下改用button-up dynamic programming
//113.08M  40ms   79.91MB 2.27K  6.09MB 5/29 21:52:08
//81.22M   20ms   77.78MB 1.89 K 4.59MB 5/29 23:22:01 //改用loop
package main
import (
    "github.com/ugorji/go/codec"
    "os"
//    "fmt"
)
var targets []int64
var dp_max  []int64
var dp_shoot []int
var dp_route []int

func dp_chose(index int){
    lenth:=len(targets)
    for i:=index;i<lenth;i=dp_shoot[i]{
        if targets[i]!=0{
            dp_route=append(dp_route,int(i+1))
        }
    }
}

func dp_build(targets_len int){
    for i:=targets_len-4;i>=0;i--{
        index2:=(i+2)&3
        index3:=(i+3)&3
        if dp_max[index2]>dp_max[index3]{
            dp_shoot[i]=i+2
            dp_max[i&3]=targets[i]+dp_max[index2]
        }else{
            dp_shoot[i]=i+3
            dp_max[i&3]=targets[i]+dp_max[index3]
        }
    }
}

func dp_init(targets_len int){
    index1:=targets_len-1
    dp_max[index1&3]=targets[index1]
    dp_shoot[index1]=targets_len
    if targets_len>=2{
        index2:=targets_len-2
        dp_max[index2&3]=targets[index2]
        dp_shoot[index2]=targets_len
    }
    if targets_len>=3{
        index3:=targets_len-3
        dp_max[index3&3]=targets[index1]+targets[index3]
        dp_shoot[index3]=index1
    }
}


func main(){
    input,_:=os.Open("input.txt")
    output,_:=os.Create("output.txt")
    defer input.Close()
    defer output.Close()
    var msgh codec.MsgpackHandle
    var handler=&msgh
    dec := codec.NewDecoder(input,handler)
    enc := codec.NewEncoder(output,handler)

    var count int
    dec.Decode(&count)

    for i:=0;i<count;i++{
        var max int64
        dec.Decode(&targets)
        targets_len := len(targets)
        dp_shoot= make([]int,targets_len)
        dp_max= make([]int64,4)
        dp_route= make([]int,0)
        go dp_init(targets_len)
        go dp_build(targets_len)
        if targets_len>1{
            if dp_max[1]>dp_max[0]{
                max=dp_max[1]
                dp_chose(1)
            }else{
                max=dp_max[0]
                dp_chose(0)
            }
        }else {
            max=dp_max[0]
            dp_chose(0)
        }
//        fmt.Println(max,dp_route)
        enc.Encode(max)
        enc.Encode(dp_route)
    }
}
