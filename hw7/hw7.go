package main
import (
    "github.com/ugorji/go/codec"
    "os"
        //"fmt"
)
var targets []float64
var total_prob[]float64
var item_prob []float64

func hanoi(len int)float64{
    var expect float64=0
    item_prob=make([]float64,len)
    var tmp float64=0
    total_prob[len]=1.0
    for i:=len-1;i>=0;i--{
        for j:=i;j<=len-1;j++{
            tmp=total_prob[j+1]/float64(j+1);
            total_prob[j]+=tmp
            item_prob[i]+=tmp
        }
    }
    for i:=0;i<len;i++{
        expect+=item_prob[i]*targets[i]
    }
    return expect;
}


func main(){
    input,_:=os.Open("input.txt")
    output,_:=os.Create("output.txt")
    defer input.Close()
    defer output.Close()
    var msgh codec.MsgpackHandle
    var handler=&msgh
    dec := codec.NewDecoder(input,handler)
    enc := codec.NewEncoder(output,handler)

    var count int
    dec.Decode(&count)

    for count>0{
        dec.Decode(&targets)
        targets_len := len(targets)
        total_prob=make([]float64,301)
        enc.Encode(hanoi(targets_len))
        count--
    }
}
