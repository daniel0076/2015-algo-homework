//I am soooo poor at DP WTF
//Until now, the homework spent me
//Total days wasted: 3days
//Total hours wasted: 28 hours
#include<iostream>
#include<vector>
#include<msgpack.hpp>
using namespace std;
using namespace msgpack;

const static short size=300;
//double proba[size][size]={0};
vector<double> proba[size];



inline void ReadInput(unpacker& unpack,string fname){
    FILE* fin = fopen(fname.c_str(),"rb");
    fseek(fin,0,SEEK_END);
    int fsize=(ftell(fin));
    rewind(fin);
    unpack.reserve_buffer(fsize);
    fread(unpack.buffer(),fsize,1,fin);
    unpack.buffer_consumed(fsize);
    fclose(fin);
}
inline void WriteOutput(sbuffer &sbuff,string fname){
    FILE* fout = fopen(fname.c_str(),"wb");
    fwrite(sbuff.data(),sbuff.size(),1,fout);
    fclose(fout);
}
inline void build(){
    for(short i=0;i<size;++i){
            proba[i].resize(i+1);
            proba[i][i]=1.0/(i+1);
        for(short j=i-1;j>=0;--j){
            proba[i][j]=proba[i-1][j]/(i+1)+proba[i][j+1];
        }
    }
}

vector<short> targets;

inline double solve(short len){
    double expect=0;
    for(short i=0;i<len;++i){
        expect+=proba[len-1][i]*targets[i];
    }
    return expect;
}

int main(){
    build();
    short count;
    unpacker unpack;
    unpacked res;
    sbuffer sbuff;
    ReadInput(unpack,"input.txt");
    unpack.next(&res);
    res.get().convert(&count);
    while(count--){
        unpack.next(&res);
        res.get().convert(&targets);
        pack(&sbuff,solve(targets.size()));
    }
    WriteOutput(sbuff,"output.txt");
    return 0;
}
