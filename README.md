Algorithm Homework
===

Online Judge at : https://codesensor.tw

Introduction to Algorithm NCTU 2015 Spring

---

### Description

+ I/O using MessagePack : http://msgpack.org
+ Mainly using Golang

---

+ HW1: Sorting (text I/O)
    + CountingSort / QuickSort / RadixSort / HeapSort
+ HW2: Sorting (MsgPack binary I/O)
    + QuickSort / RadixSort / HeapSort
+ HW3: Numbers of Inversions
    + Based on MergeSort
+ HW4: Distinct Patterns
    + Hash and Search
+ HW5: Range Minimum Query
    + Based on binary search (Can also be done by using RB-Tree)
+ HW6: Shooter
    + DP (One dimension array)
+ HW7: Hanoi Tower
    + DP (Two dimension array)
+ HW9: Hanoi Tower (Big Testdata)
    + DP (Two dimension array)
