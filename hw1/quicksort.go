//363.87 M  135.65 K    49.66 MB 3/1 09:34:45
//363.87 M  132.05 K    49.66 MB 3/2 00:06:22
//363.82 M  125.06 K    49.66 MB 3/4 16:24:41
package main

import (
    "fmt";
    "os";
    "bufio";
    "strconv";
    "strings";
)
func quicksort(list []int,left int,right int)[]int{
    if left < right {
        l := left +1
        r := right
        pivot:= list[left]
        for l<=r  {
            for list[l]<=pivot {
                l+=1
                if l>=right {
                    break
                }
            }

            for list[r]>=pivot {
                r-=1
                if r<=left{
                    break
                }
            }
            if l<r {
                list[r],list[l]=list[l],list[r]
            }else {break}
        }
        list[left],list[r]=list[r],list[left]
        list=quicksort(list,left,r-1)
        list=quicksort(list,r+1,right)
    }
    return list
}

func main(){
    file,err1:=os.Open("input.txt")
    if err1 != nil{
        fmt.Println("error opening file")
    }
    output,err2:=os.OpenFile("output.txt",os.O_CREATE|os.O_WRONLY,0664)
    if err2 != nil{
        fmt.Println("error opening file")
    }
    defer file.Close()
    defer output.Close()
    reader := bufio.NewReaderSize(file,2048*1024)
    reader.ReadString('\n')
    for {
        raw,_,err:=reader.ReadLine()
        if err !=nil{
            break
        }
        raw_s:=string(raw)
        var str []string=strings.Split(raw_s," ")
        var arr []int
        n:=len(str)
        for i:=0;i<n;i++{
            _int,e:=strconv.Atoi(str[i])
            if e == nil{
                arr=append(arr,_int)
            }
        }
        arr_n:=len(arr)
        arr=quicksort(arr,0,arr_n-1)
        for i:=0;i<arr_n-1;i++{
            output.WriteString(strconv.Itoa(arr[i]))
            output.WriteString(" ")
        }
        output.WriteString(strconv.Itoa(arr[arr_n-1]))
        output.WriteString("\n")
    }
}
