def heap_adjust(arr,key,l):
    root=key
    while True:
        child=root*2+1
        if child > l: break
        if child+1 <= l and arr[child] < arr[child+1]:
            child+=1
        if arr[root] < arr[child]:
            arr[root],arr[child]=arr[child],arr[root]
            root=child
        else : break



def heapsort(arr):
    for i in range( int(len(arr)/2)-1 , -1 , -1):
        heap_adjust(arr,i,len(arr)-1)
    for j in range( len(arr)-1 , 0 , -1):
        arr[0],arr[j]=arr[j],arr[0]
        heap_adjust(arr,0,j-1)


with open('input.txt') as f,open('output.txt','w')as o:
    f.readline()
    data=f.readlines()
    for l in data:
        line=[int(n) for n in l.split()]
        heapsort(line)
        o.write(" ".join(str(n) for n in line))
        o.write('\n')


