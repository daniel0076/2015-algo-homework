#!/usr/bin/env python


with open('input.txt') as f,open('output.txt','w')as o:
    f.readline()
    data=f.readlines()
    for l in data:
        line=[int(n) for n in l.split()]
        line=sorted(line)
        o.write(" ".join(str(n) for n in line))
        o.write('\n')

