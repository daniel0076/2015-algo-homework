//383.83 M 120.65 K 50.72 MB 5.46 MB 3/4 20:15:47
//300.89 M  83.54 K 51.78 MB 5.74 MB 3/4 21:35:57
//187.35 M  51.05 K 51.23 MB 5.43 MB 3/5 01:05:07
package main

import (
    "os";
    "bufio";
    "strconv";
    "strings";
    "bytes";
)
func countingsort(arr []int)[]int{
    n:=len(arr)
    var res []int
    count:=make([]int,1200)
    for i:=0;i<n;i++{
        count[arr[i]]++
    }
    for j:=0;j<1200;j++ {
        for count[j]!=0 {
            res=append(res,j)
            count[j]--
        }
    }
    return res
}

func main(){
    file,_:=os.Open("input.txt")
    output,_:=os.OpenFile("output.txt",os.O_CREATE|os.O_WRONLY,0664)
    defer file.Close()
    defer output.Close()
    reader := bufio.NewReaderSize(file,2048*1024)
    reader.ReadString('\n')
    for {
        raw,_,err:=reader.ReadLine()
        if err !=nil{
            break
        }
        raw_s:=string(raw)
        var str []string=strings.Split(raw_s," ")
        var arr []int
        n:=len(str)
        for i:=0;i<n;i++{
            _int,_:=strconv.Atoi(str[i])
            arr=append(arr,_int)
        }
        arr=countingsort(arr)
        arr_n:=len(arr)
        var out_s bytes.Buffer
        for i:=0;i<arr_n-1;i++{
            out_s.WriteString(strconv.Itoa(arr[i])+" ")
        }
            out_s.WriteString(strconv.Itoa(arr[arr_n-1])+"\n")
            output.WriteString(out_s.String())
    }
}
