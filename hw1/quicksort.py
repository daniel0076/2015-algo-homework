#!/usr/bin/env python3


def quicksort(arr,left,right):
    if(left<right):
        l=left+1
        r=right
        pivot=arr[left]
        while(l<=r):
            while(arr[l]<=pivot):
                l=l+1
                if(l>=right):
                   break
            while(arr[r]>=pivot):
                r=r-1
                if(r<=left):
                    break
            if(l<r):
                arr[l],arr[r]=arr[r],arr[l]
            else:
                 break

        arr[left],arr[r]=arr[r],arr[left]
        quicksort(arr,left,r-1)
        quicksort(arr,r+1,right)

with open('input.txt') as f,open('output.txt','w')as o:
    f.readline()
    data=f.readlines()
    for l in data:
        line=[int(n) for n in l.split()]
        quicksort(line,0,len(line)-1)
        o.write(" ".join(str(n) for n in line))
        o.write('\n')
