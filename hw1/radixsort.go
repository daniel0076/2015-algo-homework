//383.83 M 120.65 K 50.72 MB 5.46 MB 3/4 20:15:47
//300.89 M  83.54 K 51.78 MB 5.74 MB 3/4 21:35:57
package main

import (
    "os";
    "bufio";
    "strconv";
    "strings";
    "bytes";
)
func radixsort(arr []int)[]int{
    largest:=0
    n:=len(arr)
    for i:=0;i<n;i++{
        if (arr[i])>largest {
            largest=arr[i]
        }
    }
    radix:=1
    count:= [10]int{0}
    bucket:=make([][]int,10)
    for radix<largest{
        for i:=0;i<n;i++ {
            lsd:=int((arr[i]/radix)%10)
            bucket[lsd]=append(bucket[lsd],arr[i])
            count[lsd]++
        }
        radix*=10
        dataindex:=0
        for j:=0;j<10;j++{
            if count[j]!=0{
                for k:=0;k<count[j];k++ {
                    arr[dataindex]=bucket[j][k]
                    dataindex++
                }
            }
            count[j]=0
        }
        bucket=make([][]int,10)
    }
    return arr
}

func main(){
    file,_:=os.Open("input.txt")
    output,_:=os.OpenFile("output.txt",os.O_CREATE|os.O_WRONLY,0664)
    defer file.Close()
    defer output.Close()
    reader := bufio.NewReaderSize(file,2048*1024)
    reader.ReadString('\n')
    for {
        raw,_,err:=reader.ReadLine()
        if err !=nil{
            break
        }
        raw_s:=string(raw)
        var str []string=strings.Split(raw_s," ")
        var arr []int
        n:=len(str)
        for i:=0;i<n;i++{
            _int,_:=strconv.Atoi(str[i])
            arr=append(arr,_int)
        }
        arr=radixsort(arr)
        arr_n:=len(arr)
        var out_s bytes.Buffer
        for i:=0;i<arr_n-1;i++{
            out_s.WriteString(strconv.Itoa(arr[i])+" ")
        }
            out_s.WriteString(strconv.Itoa(arr[arr_n-1])+"\n")
            output.WriteString(out_s.String())
    }
}
