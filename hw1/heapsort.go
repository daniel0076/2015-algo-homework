package main

import(
    "fmt";
    "bufio";
    "os";
    "strings";
    "strconv";
)


func heapify(list []int,key int,l int) []int {
    root:=key
    for  {
        child := root * 2 + 1
        if child >=l  {
            break
        }
        if child+1 < l && list[child] < list [child+1]{
            child+=1
        }
        if list[root] < list[child]{
            list[root],list[child]=list[child],list[root]
            root=child
        }else {
            break
        }
    }
    return list
}

func heapsort(list []int)[]int{
    for i :=int(len(list)/2)-1;i >=0;i--{
        list=heapify(list,i,len(list))
    }
    for i :=len(list)-1;i >0;i--{
        list[0],list[i]=list[i],list[0]
        list=heapify(list,0,i)
    }
    return list
}

func main(){
    file,err1:=os.Open("input.txt")
    if err1 != nil{
        fmt.Println("error opening file")
    }
    output,err2:=os.OpenFile("output.txt",os.O_CREATE|os.O_WRONLY,0664)
    if err2 != nil{
        fmt.Println("error opening file")
    }
    scanner := bufio.NewScanner(file)
    scanner.Scan()
    for scanner.Scan(){
        var str []string=strings.Split(scanner.Text()," ")
        var arr []int
        for i:=0;i<len(str);i++{
            _int,e:=strconv.Atoi(str[i])
            if e == nil{
                arr=append(arr,_int)
            }
        }
        heapsort(arr)
        for i:=0;i<len(arr)-1;i++{
            output.WriteString(strconv.Itoa(arr[i]))
            output.WriteString(" ")
        }
        output.WriteString(strconv.Itoa(arr[len(arr)-1]))
        output.WriteString("\n")

    }
    defer file.Close()
    defer output.Close()
}
