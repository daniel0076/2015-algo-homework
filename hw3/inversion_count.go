package main
import (
    "github.com/ugorji/go/codec"
    "os"
    "bufio"
)

func mergesort(arr []int,result []int,left int,right int) int {
    inv_count:=0
    if(left<right){
        mid:=int((left+right)/2)
        inv_count+=mergesort(arr,result,left,mid)
        inv_count+=mergesort(arr,result,mid+1,right)
        inv_count+=merge(arr,result,left,mid+1,right)
    }
    return inv_count
}
func merge(arr []int,result []int,left int,mid int,right int) int {
    l:=left
    r:=mid
    k:=left
    inv_count :=0
    for (l<=mid-1) && (r<=right){
        if arr[l]<=arr[r] {
            result[k]=arr[l];
            k++
            l++
        }else {
            result[k]=arr[r]
            k++
            r++
            inv_count =inv_count + (mid-l)
        }
    }
    for l<=mid-1{
        result[k]=arr[l]
        k++
        l++
    }
    for r<=right{
        result[k]=arr[r]
        k++
        r++
    }
    for j:=left;j<=right;j++{
        arr[j]=result[j]
    }
    return inv_count
}

func main(){
    file,_:=os.Open("input.txt")
    output,_:=os.OpenFile("output.txt",os.O_CREATE|os.O_WRONLY,0664)
    defer file.Close()
    defer output.Close()
    reader := bufio.NewReaderSize(file,2048*1024)
    var msgh codec.MsgpackHandle
    var handler=&msgh
    dec := codec.NewDecoder(reader,handler)
    enc := codec.NewEncoder(output,handler)
    var count int
    var arr []int
    result := make([]int,len(arr))
    dec.Decode(&count)
    for i:=0;i<count;i++{
        dec.Decode(&arr)
        enc.Encode(mergesort(arr,result,0,len(arr)-1))
    }
}
