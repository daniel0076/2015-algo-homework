//17.98G 6s 810.82MB 8.68K  738.35MB 4/15 12:29:21
//8.56G  4s 187.98MB 9.39K  109.64MB 4/15 14:14:24

package main
import (
    "github.com/ugorji/go/codec"
    "os"
    "fmt"
    "math"
)

var MAX uint64 = 1000000
var CARRY uint64 = 59
func hash(pattern string,last uint64,base uint64,sub_len uint64)uint64{
    var index uint64=0
    if sub_len==1{
        index=uint64(pattern[0]-'A')
    } else if last==0{
        for i:=0;i<len(pattern);i++ {
            index=index*CARRY+uint64(pattern[i]-'A')
        }
    }else {
        index=(last % base)*CARRY+uint64(pattern[sub_len-1]-'A')
    }
    for index >= MAX {
        index=index %  MAX + index/MAX
    }


    return index
}
func hashqq(pattern string,last uint64,base uint64,sub_len uint64)uint64{
    var index uint64=0
    for i:=uint64(0);i<sub_len;i++ {
        index=(index*CARRY+uint64(pattern[i]-'A'))
    }
    for index >= MAX {
        index=index %  MAX + index/MAX
    }

    return index
}
func main(){
    input,_:=os.Open("input3.txt")
    output,_:=os.Create("output.txt")
    defer input.Close()
    defer output.Close()
    var msgh codec.MsgpackHandle
    var handler=&msgh
    dec := codec.NewDecoder(input,handler)
//    enc := codec.NewEncoder(output,handler)

    var count uint64
    var sub_len uint64
    var dif_len uint64
    var str string
    var pattern string
    var total uint64 = 0
    var base uint64=0
    dec.Decode(&count)
    for i:=uint64(0);i<count;i++{
        dec.Decode(&sub_len)
        dec.Decode(&dif_len)
        dec.Decode(&str)

        bucket:=make([][]string,MAX)
        for i := uint64(0);i+sub_len<= uint64(len(str));i++ {
            index := uint64(0)
            base=uint64(math.Pow(float64(CARRY),float64(sub_len)))
            pattern=str[i:i+sub_len]
            index=hash(pattern,index,base,sub_len)
            //fmt.Println(pattern,index,sub_len,total,i,len(str),len(bucket))
            var found bool =false
            for j:=range bucket[index]{
                if pattern==bucket[index][j] {
                    found=true
                    break
                }
            }
            if(!found){
                bucket[index]=append(bucket[index],pattern)
                total++
            }
        }
        //    enc.Encode(total)
        fmt.Println(total)
        total=0
    }
}
