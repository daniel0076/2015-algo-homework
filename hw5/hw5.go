//4.73G 2s 87.69MB golang 4.62K 15.39MB 5/2 00:00:37
package main
import (
    "github.com/ugorji/go/codec"
    "os"
//    "fmt"
)

func quicksort(list []int64,left int64,right int64)[]int64{
    if left < right {
        l := left +2
        r := right
        pivot:= list[left]
        for l<=r  {
            for list[l]<=pivot {
                l+=2
                if l>=right {
                    break
                }
            }

            for list[r]>=pivot {
                r-=2
                if r<=left{
                    break
                }
            }
            if l<r {
                list[r],list[l]=list[l],list[r]
                list[r+1],list[l+1]=list[l+1],list[r+1]
            }else {break}
        }
        list[left],list[r]=list[r],list[left]
        list[left+1],list[r+1]=list[r+1],list[left+1]
        list=quicksort(list,left,r-2)
        list=quicksort(list,r+2,right)
    }
    return list
}

func clean(candidates []int64)[]int64{
    var max int64=-1
    var result []int64
    length:=int64(len(candidates))
    for i:=int64(0) ;i<length-1;i=i+2{
        if candidates[i+1]>max{
            max=candidates[i+1]
            result=append(result,candidates[i],candidates[i+1])
        }
    }
    return result
}

func minimal_query(candidates []int64,query []int64)[]int64{
    min_range := []int64{-1,-1}
    length:=int64(len(candidates))-1
    index:=((length-1)/2)+((length-1)/2)%2
    end:=length-1
    start:=int64(0)
    for{
        //fmt.Println(start,index,end,"loop",candidates[index:index+2],query,min_range)
        if query[0]<=candidates[index+1] && query[1] >= candidates[index]  { //in range
            //fmt.Println(start,index,end,"inrange",candidates[index:index+2],query,min_range)
            if min_range[0] == -1{
                min_range=candidates[index:index+2]
            } else if candidates[index]<=min_range[0] && candidates[index+1] <= min_range[1]{
                min_range=candidates[index:index+2]
            }else if min_range[0]==candidates[index] && min_range[1]==candidates[index+1]{
                break
            }
        }
        if index==end || index == start{
            break
        }
        if query[0]<candidates[index+1]{
            end=index
            index=(start+end)/2-((start+end)/2)%2
            //fmt.Println(start,index,end,"decrease",candidates[index:index+2],query)
        }else{
            start=index
            index=(start+end)/2+((start+end)/2)%2
            //fmt.Println(start,index,end,"increase",candidates[index:index+2],query)
        }
    }
    return min_range
}


func main(){
    input,_:=os.Open("input.txt")
    output,_:=os.Create("output.txt")
    defer input.Close()
    defer output.Close()
    var msgh codec.MsgpackHandle
    var handler=&msgh
    dec := codec.NewDecoder(input,handler)
    enc := codec.NewEncoder(output,handler)

    var count int64
    dec.Decode(&count)

    for i:=int64(0);i<count;i++{
        var candidates []int64
        var query_set  []int64
        var result []int64
        dec.Decode(&query_set)
        dec.Decode(&candidates)
        query_len:=int64(len(query_set))
        candidates=quicksort(candidates,0,int64(len(candidates)-2))
        candidates=clean(candidates)
        for j:=int64(0);j<query_len-1;j=j+2{
            tmp:=minimal_query(candidates,query_set[j:j+2])
            result=append(result,tmp[0],tmp[1])
        }
        //fmt.Println(result)
        enc.Encode(result)
    }
}
