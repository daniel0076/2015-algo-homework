package main
import (
    "github.com/ugorji/go/codec"
    "os"
    "fmt"
)

func main(){
    input,_:=os.Open("output.msgpack")
    defer input.Close()
    var msgh codec.MsgpackHandle
    var handler=&msgh
    dec := codec.NewDecoder(input,handler)

    var ans []int64
    for i:=0;i<10;i=i+1{
        dec.Decode(&ans)
        fmt.Println(ans)
    }
}
