package main

import(
    "github.com/ugorji/go/codec"
    "os"
    "bufio"
)


func heapify(list []int,key int,l int) []int {
    root:=key
    for  {
        child := root * 2 + 1
        if child >=l  {
            break
        }
        if child+1 < l && list[child] < list [child+1]{
            child+=1
        }
        if list[root] < list[child]{
            list[root],list[child]=list[child],list[root]
            root=child
        }else {
            break
        }
    }
    return list
}

func heapsort(list []int)[]int{
    for i :=int(len(list)/2)-1;i >=0;i--{
        list=heapify(list,i,len(list))
    }
    for i :=len(list)-1;i >0;i--{
        list[0],list[i]=list[i],list[0]
        list=heapify(list,0,i)
    }
    return list
}
func main(){
    file,_:=os.Open("input.txt")
    output,_:=os.OpenFile("output.txt",os.O_CREATE|os.O_WRONLY,0664)
    defer file.Close()
    defer output.Close()
    reader := bufio.NewReaderSize(file,2048*1024)
    var msgh codec.MsgpackHandle
    var handler=&msgh
    dec := codec.NewDecoder(reader,handler)
    enc := codec.NewEncoder(output,handler)
    var count int
    var arr []int
    dec.Decode(&count)
    for i:=0;i<count;i++{
        dec.Decode(&arr)
        enc.Encode(heapsort(arr))
    }
}

