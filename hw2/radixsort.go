package main
import (
    "fmt"
    "github.com/ugorji/go/codec"
    "os"
    "bufio"
)

func radixsort(arr []int)[]int{
    largest:=0
    n:=len(arr)
    for i:=0;i<n;i++{
        if (arr[i])>largest {
            largest=arr[i]
        }
    }
    radix:=1
    count:= [10]int{0}
    bucket:=make([][]int,10)
    for radix<largest{
        for i:=0;i<n;i++ {
            lsd:=int((arr[i]/radix)%10)
            bucket[lsd]=append(bucket[lsd],arr[i])
            count[lsd]++
        }
        radix*=10
        dataindex:=0
        for j:=0;j<10;j++{
            if count[j]!=0{
                for k:=0;k<count[j];k++ {
                    arr[dataindex]=bucket[j][k]
                    dataindex++
                }
            }
            count[j]=0
        }
        bucket=make([][]int,10)
    }
    return arr
}

func main(){
    file,err1:=os.Open("input.txt")
    if err1 != nil{
        fmt.Println("error opening file")
    }
    output,err2:=os.OpenFile("output.txt",os.O_CREATE|os.O_WRONLY,0664)
    if err2 != nil{
        fmt.Println("error opening file")
    }
    defer file.Close()
    defer output.Close()
    reader := bufio.NewReaderSize(file,2048*1024)
    var msgh codec.MsgpackHandle
    var handler=&msgh
    dec := codec.NewDecoder(reader,handler)
    enc := codec.NewEncoder(output,handler)
    var count int
    var arr []int
    dec.Decode(&count)
    for i:=0;i<count;i++{
        dec.Decode(&arr)
        enc.Encode(radixsort(arr))
    }
}
