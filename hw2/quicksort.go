package main
import (
    "github.com/ugorji/go/codec"
    "os"
    "bufio"
)

func quicksort(list []int,left int,right int)[]int{
    if left < right {
        l := left +1
        r := right
        pivot:= list[left]
        for l<=r  {
            for list[l]<=pivot {
                l+=1
                if l>=right {
                    break
                }
            }

            for list[r]>=pivot {
                r-=1
                if r<=left{
                    break
                }
            }
            if l<r {
                list[r],list[l]=list[l],list[r]
            }else {break}
        }
        list[left],list[r]=list[r],list[left]
        list=quicksort(list,left,r-1)
        list=quicksort(list,r+1,right)
    }
    return list
}

func main(){
    file,_:=os.Open("input.txt")
    output,_:=os.OpenFile("output.txt",os.O_CREATE|os.O_WRONLY,0664)
    defer file.Close()
    defer output.Close()
    reader := bufio.NewReaderSize(file,2048*1024)
    var msgh codec.MsgpackHandle
    var handler=&msgh
    dec := codec.NewDecoder(reader,handler)
    enc := codec.NewEncoder(output,handler)
    var count int
    var arr []int
    dec.Decode(&count)
    for i:=0;i<count;i++{
        dec.Decode(&arr)
        enc.Encode(quicksort(arr,0,len(arr)-1))
    }
}
