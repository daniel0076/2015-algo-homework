#include <iostream>
#include <vector>
#include <queue>
#include<msgpack.hpp>

using namespace std;
using namespace msgpack;

inline void ReadInput(unpacker& unpack,string fname){
    FILE* fin = fopen(fname.c_str(),"rb");
    fseek(fin,0,SEEK_END);
    int fsize=(ftell(fin));
    rewind(fin);
    unpack.reserve_buffer(fsize);
    fread(unpack.buffer(),fsize,1,fin);
    unpack.buffer_consumed(fsize);
    fclose(fin);
}
inline void WriteOutput(sbuffer &sbuff,string fname){
    FILE* fout = fopen(fname.c_str(),"wb");
    fwrite(sbuff.data(),sbuff.size(),1,fout);
    fclose(fout);
}
vector<short> test[2];

class graph{
    public:
        graph(const short n=0){
            g.resize(n);
            record=new short[n]{0};
            parent=new short[n]{0};
            ver=new bool[n]{0};
            test[0].clear();
            test[1].clear();
        }
        ~graph(){
            delete[] record;
            delete[] ver;
            delete[] parent;
        }
        bool insert(short,short);
        void read(vector<short>&);
        void pack(sbuffer&);
        void solve();
    private:
        vector< vector<short> > g;
        short* record;
        short* parent;
        bool* ver;

};

void graph::read(vector<short>&  data){
    int total=data.size()-1;
    while(total>-1){
        insert(data[total-1],data[total]);
        total-=2;
    }
}

void graph::solve(){
    queue<short> q;
    short v;
    const int size=g.size();
    for(int i=0;i<size;++i){
        if(g[i].empty()){
            test[0].push_back(i);
            record[i]=2;
            continue;
        }else if(record[i]!=2){
            q.push(i);
            parent[i]=i;
            ver[i]=1;
            while(!q.empty()){
                v=q.front();
                ver[v]=!ver[parent[v]];
                test[ver[v]].push_back(v);
                for(int j=0;j<int(g[v].size());++j) {
                    register short curr=g[v][j];
                    if(record[curr]<1){
                        q.push(curr);
                        record[curr]=1;
                    }
                    if(record[curr]!=2&&parent[curr]!=0){
                        if(ver[v]!=ver[parent[curr]]){
                            test[0].clear();
                            test[1].clear();
                            return;
                        }
                    }
                    parent[curr]=v;
                }
                record[v]=2;
                q.pop();
            }
        }
    }
}

bool graph::insert(short n=0,short v=0){
    g[n].push_back(v);
    g[v].push_back(n);
    return 1;
}


int main(){
    short count;
    unpacker unpack;
    unpacked res;
    sbuffer sbuff;
    ReadInput(unpack,"input.txt");
    unpack.next(&res);
    res.get().convert(&count);
    vector<short> input;
    short stu;
    while(count--){
        unpack.next(&res);
        res.get().convert(&stu);
        unpack.next(&res);
        res.get().convert(&input);
        graph sol(stu);
        sol.read(input);
        sol.solve();
        pack(&sbuff,test[0]);
        pack(&sbuff,test[1]);
    }
    WriteOutput(sbuff,"output.txt");
    return 0;
}
