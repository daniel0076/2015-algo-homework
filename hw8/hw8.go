package main

import (
    "github.com/ugorji/go/codec"
    "os"
//    "//fmt"
)

var graph [][]int;
var ver []int8;
var record []int8;
var test [][]int;
var data []int;

func insert(n int,v int){
    graph[n]=append(graph[n],v);
    graph[v]=append(graph[v],n);
}

func initiate(n int){
    graph=make([][]int,n);
    ver=make([]int8,n);
    record=make([]int8,n);
    test=make([][]int,2);
    test[0]=make([]int,0);
    test[1]=make([]int,0);
}

func solve(){
    queue:=make([]int,0)
    length:=len(graph)
    for i:=0;i<length;i++ {
        if len(graph[i])==0{
            test[0]=append(test[0],i);
            record[i]=2;
            continue;
        }else if record[i]==0{
            queue=append(queue,i); //init push
            test[0]=append(test[0],i); //give test
            //fmt.Printf("%v pushed got 0\n",i);
            ver[i]=0;
            for len(queue)>0{
                v:=queue[0]
                record[v]=2
                queue=append(queue[:0],queue[1:]...);//pop
                //fmt.Printf("%v popped, queue=%v\n",v,queue)
                //fmt.Printf("now at %v start to loop\n",v);
                for j:=0;j<len(graph[v]);j++{
                    curr:=graph[v][j];
                    //fmt.Printf("now at %v,loop to %v\n",v,curr);
                    if record[curr]!=2 {
                        if record[curr]==0{ //沒拿考卷
                            queue=append(queue,curr); //push
                            if ver[v]==0 {
                                ver[curr]=1
                                test[1]=append(test[1],curr); //give test
                                //fmt.Printf("%v got 1\n",curr);
                            }else{
                                ver[curr]=0
                                test[0]=append(test[0],curr); //give test
                                //fmt.Printf("%v got 0\n",curr);
                            }
                            record[curr]=1;
                        }else{
                            if ver[v]==ver[curr]{//error
                                //fmt.Println("error")
                                test[0]=make([]int,0);
                                test[1]=make([]int,0);
                                return
                            }
                        }
                    }
                }
            }
        }
    }
}

func read(data []int){
    total:=len(data)-1;
    for total>-1 {
        insert(data[total-1],data[total]);
        total-=2;
    }
}

func main(){

    input,_:=os.Open("input.txt")
    output,_:=os.Create("output.txt")
    defer input.Close()
    defer output.Close()
    var msgh codec.MsgpackHandle
    var handler=&msgh
    dec := codec.NewDecoder(input,handler)
    enc := codec.NewEncoder(output,handler)

    var count int
    var stu int;
    dec.Decode(&count)

    for i:=0;i<count;i++{
        dec.Decode(&stu);
        dec.Decode(&data);
        initiate(stu);
        read(data);
        solve()
        enc.Encode(test[0]);
        enc.Encode(test[1]);
    }
    //initiate(4);
    //read([]int{0,1,0,2,1,3,2,3})
    //solve();
    ////fmt.Println(test[0]);
    ////fmt.Println(test[1]);
}
